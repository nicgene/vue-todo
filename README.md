# vue-todo
A todo lists and todo items app using a [serverless REST api](https://gitlab.com/dupkey/serverless-examples/tree/master/aws-node-rest-api-with-dynamodb-and-offline) for the back-end. This Vue.js project uses vue-router and axios.

## Project setup
```
yarn install
```

### Compiles and hot-reloads for development
```
yarn run serve
```

### Compiles and minifies for production
```
yarn run build
```

### Lints and fixes files
```
yarn run lint
```

